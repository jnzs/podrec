#!/bin/sh
# Given a file pattern, generates spectrograms for all audio files 
# matching that pattern

files=$1
out_dir=./spects/medium4k
spect_flags="-r -m -s -x 500 -y 257"

for f in $files
do
  f_prefix=$(echo $f | sed 's/\.mp3//i' | sed 's/.*\///i')
  outfile="$f_prefix.png"
  
  #special case for bugle podcasts TODO fix noisiness of this...
  #lame --decode $f temp.wav &&
  #sox temp.wav temp2.wav remix 1-2 &&
  #sox temp2.wav -n spectrogram $spect_flags -o $out_dir/$outfile

  #standard case
  channels=$(sox --info -c $f)
  if [ $channels -eq 2 ] ; then
    sox $f temp.mp3 remix 1-2 && 
    sox temp.mp3 -n spectrogram $spect_flags -o $out_dir/$outfile
  else
    sox $f -n spectrogram $spect_flags -o $out_dir/$outfile
  fi
  
  echo "$outfile from $f" 
done
