# README for the PodRec Podcast Recogniser #
# By Jacob Soderlund, 2016 #

### Dependencies ###
Anaconda (Python 3.5 version),  
TensorFlow  
wget (for downloading podcast episodes)  
mp3cut (for generating snippets)  
sox (for generating spectrograms)  

### Setting up PodRec ###
Before you run PodRec, you need to download data to train it on by following these steps:  
1. Download episodes  
2. Generate snippets  
3. Generate spectrograms  


### Training the PodRec Podcast Recogniser ###

To train a model to recognise podcasts (with default settings), just run podrec_2ConvCNN_train.py like so:  
./podrec_2ConvCNN_train.py -i <image_directory>  
  
Images should be spectrograms of 500 x 257 resolution, with file names following the pattern  
<podcast_ID>_<episode_no>_snip<snippet_no>.png  
  
Currently only the following hardcoded podcast IDs are recognised:  
behind_lines  
bugle  
campaign  
com_com_pod  
dragon_friends  
jl_show  
night_vale  
radiolab  
serial  
tofop