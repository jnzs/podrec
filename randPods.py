import sys, random, feedparser

MAX_RETRIES = 3

def get_podref(entry):
  """ Return the URL of the first mp3 audio file in this RSS entry, 
  or None if the entry has no enclosed audio files"""
  
  podref = None
  for e in entry.enclosures:
    if e['type'] == 'audio/mpeg':
      podref = e['href']
      break
  return podref

def main():
  
  if len(sys.argv) != 3:
    sys.stderr.write('Usage: randPods.py <rss/atom_feed> <num_episodes>\n')
    exit(1)
  
  # Access podcast feed
  feed = feedparser.parse( sys.argv[1] )
  if ('status' not in feed) or (feed['status'] != 200):
    sys.stderr.write('An error occurred loading feed: '+sys.argv[1]+'\n')
    exit(1)
  # Number of episodes to choose randomly
  num_eps = int( sys.argv[2] )
  #print( num_eps, "episodes in feed" )
  # List of episode numbers to choose from without replacement
  ep_nos = [x for x in range( len(feed.entries) )]
  
  for i in range (num_eps):
    podref = None
    retry_count = 0
    while podref == None and retry_count < MAX_RETRIES:
      
      if len(ep_nos) == 0:
        sys.stderr.write('Not enough podcasts in feed: '+sys.argv[1]+'\n')
        exit(1)
      
      choice = random.choice( ep_nos )
      ep_nos.remove( choice )
      entry = feed.entries[choice]
      podref = get_podref(entry)
      
      retry_count += 1
    if podref == None:
      sys.stderr.write('Could not find audio file in feed: '+sys.argv[1]+'\n') #TODO more info
    else:
      print( podref )
      
if __name__ == "__main__":
  main()
