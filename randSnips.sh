#!/bin/sh

# Given a filename pattern, takes random audio snippets from all matching
# mp3 files
files=$1
num_snips=$2   # per podcast
snip_length=$3 # in seconds
start_no=$4

out_dir="./snips4k"

if [ -z "$files" -o -z "$num_snips" -o -z "$snip_length" ]; then
  echo "usage: randSnips.sh \"<files>\" <num_snips> <snip_length>"
  exit 1;
fi

if [ -z $start_no ]; then
  start_no=0
fi

mkdir -p $out_dir

# Generate snippets for each mp3 file
for f in $files; do
  
  f_prefix=$(echo $f | sed 's/\.mp3//i' | sed 's/.*\///i')
  duration=$( mp3info -p "%S\n" $f )
  
  for n in $(seq 0 $(($num_snips-1))); do
    suffix=$(($n + $start_no))
    # Generate random start/end times for snippet
    start_snip=$(python3 -c "import sys, random, math; sec = random.randint(0, $duration-$snip_length); print(math.floor(sec/60)); print(sec%60); print(math.floor((sec+$snip_length)/60)); print((sec+$snip_length)%60)")
    
    set -- $start_snip # put 1st and 2nd elements of this into $1, $2 etc.
    #echo $1 $2 $3 $4 
    # TODO allow indices to start at a nonzero number   
    mp3cut -o $out_dir/${f_prefix}_snip$suffix.mp3 -t "$1:$2-$3:$4" $f 
  done
done
