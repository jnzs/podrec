"""
A very simple podcast spectrogram classifier.

Trains a softmax linear model to predict which podcast series a
15-second sound snippet comes from (presented as a spectrogram)
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import resize_image_patch
import re, glob, argparse

import podcast_spects as spect

#IMG_DIR = "../spects/medium"
NUM_OUTPUTS = 10
#IMG_HEIGHT = 257
#IMG_WIDTH  = 500
NUM_CHANNELS = 1 # Use greyscale images

#BATCH_SIZE = 200

#TODO next time, implement as a class? There has to be a better way to get the test and train sets to run on the same model...
def softmax_model(features, weights, biases):
  weighted_sum = (tf.matmul( features, weights ) + biases)
  output = tf.nn.softmax( weighted_sum )
  return output

def main():
  
  parser = argparse.ArgumentParser()
  parser.add_argument("-n", "--num_iterations", type=int, default=1000)
  parser.add_argument("-b", "--batch_size", type=int, default=200)
  parser.add_argument("-i", "--img_dir", default='../spects/medium', 
                      help="directory of training images")
  parser.add_argument("-x", "--img_width", default=500, 
                      type=int)
  parser.add_argument("-y", "--img_height", default=257, 
                      type=int)
  parser.add_argument("-o", "--outfile", 
                      help="file of saved model weights")

  args = parser.parse_args()
    
  # get data
  train_labels, train_files, test_labels, test_files = \
    spect.get_label_file_list(args.img_dir+"/*.png", 
                              test_on_unseen_eps=True)
  # convert to tensors
  train_imgs_t = tf.convert_to_tensor(train_files, dtype=tf.string)
  train_labs_t = tf.convert_to_tensor(train_labels, dtype=tf.float32)
  test_imgs_t  = tf.convert_to_tensor(test_files, dtype=tf.string)
  test_labs_t  = tf.convert_to_tensor(test_labels, dtype=tf.float32)
  
  # create input queue
  train_input_queue = tf.train.slice_input_producer(
    [train_imgs_t, train_labs_t], shuffle=True)
  # don't bother to shuffle the test set - we use all of it every time
  test_input_queue = tf.train.slice_input_producer(
    [test_imgs_t, test_labs_t], shuffle=False)
  
  # get single examples (flattened images)
  train_label, train_image = spect.read_and_decode_image( 
                             train_input_queue, flatten=True,
                             img_height=args.img_height, 
                             img_width=args.img_width)
  test_label, test_image = spect.read_and_decode_image(
                           test_input_queue, flatten=True,
                           img_height=args.img_height,
                           img_width=args.img_width)
  
  # group training examples into batches randomly
  print("Batch size:", args.batch_size)
  train_images_batch, train_labels_batch = tf.train.batch(
      [train_image, train_label], batch_size=args.batch_size,
      capacity=600)
  # The test batch contains the whole test set
  test_images_batch, test_labels_batch = tf.train.batch(
      [test_image, test_label], batch_size=len(test_files),
      capacity=2*len(test_files))
  
  # Construct model
  num_pixels = train_image.get_shape().as_list()[0]
  weights = tf.Variable(tf.zeros([num_pixels, NUM_OUTPUTS]))
  biases = tf.Variable(tf.zeros([NUM_OUTPUTS]))  
  train_output = softmax_model(train_images_batch, weights, biases)
  test_output  = softmax_model(test_images_batch, weights, biases)
  
  # Cross entropy (loss function)
  # NOTE: Sum is across all pixels, not all batch members
  train_x_entropy = tf.reduce_mean(
    -tf.reduce_mean(train_labels_batch * tf.log(train_output+1e-8), 1) )
  train_op = tf.train.MomentumOptimizer(0.008, 0.6).minimize(
    train_x_entropy)
  
  test_x_entropy = tf.reduce_mean(
    -tf.reduce_mean(test_labels_batch * tf.log(test_output), 1) )
  
  # Define some stuff for validation
  #correct_prediction = tf.equal(tf.argmax(train_output,1), tf.argmax(train_labels_batch,1))
  train_accuracy = tf.reduce_mean(tf.cast(
    tf.equal(tf.argmax(train_output,1), tf.argmax(train_labels_batch,1)), 
    tf.float32))
  test_accuracy = tf.reduce_mean(tf.cast(
    tf.equal(tf.argmax(test_output,1), tf.argmax(test_labels_batch,1)), 
    tf.float32))
  #weights_vis = tf.reshape(weights, [64, 64, 3, NUM_OUTPUTS])
  #weights_v1, weights_v2 = tf.split(3, 2, weights_vis)
  # Use the graph and compute the model
  
  init = tf.initialize_all_variables()
  
  with tf.Session() as sess:
    # First initialise tf variables
    sess.run(init)
    
    # initialise queue runners and coordinator
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    
    # Iterate
    for i in range(args.num_iterations):
      _, cost, acc = sess.run([train_op, train_x_entropy, train_accuracy])
      if i%20 == 0: 
        print("Iteration", i, ":", cost)
      if i%100 == 0:
        test_cost, test_acc = sess.run([test_x_entropy, test_accuracy])
        print("Training accuracy (estimate):", str(acc*100)+'%')
        print("Test accuracy:", str(test_acc*100)+'%')
    print("Finished at", i+1, "iterations")
    
    # test/validate model on test data
    cost, acc, _, _ = sess.run([train_x_entropy, train_accuracy, train_labels_batch, train_output])
    test_cost, test_acc = sess.run([test_x_entropy, test_accuracy])
    print("Final training cost (estimate):", cost)
    print("Final test cost:", test_cost)
    print("Final training accuracy (estimate):", str(acc*100)+'%')
    print("Final test accuracy:", str(test_acc*100)+'%')
    
    #print("----- Random Target Outputs: -----")
    #print(tar[0])
    #print(tar[1])
    #print(tar[2])
    #print(tar[3])
    #print(tar[4])
    #print("----- Random Actual Outputs: -----")
    #print(out[0])
    #print(out[1])
    #print(out[2])
    #print(out[3])
    #print(out[4])
    
    # unitialise queue runners and coordinator
    coord.request_stop()
    coord.join(threads)

if __name__ == "__main__":
  main()
