""" 
Contains helper functions to convert between the name of a podcast and 
its representation as a one-hot vector 
"""
import sys, re

encodings = {
  "behind_lines"   : [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  "bugle"          : [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
  "campaign"       : [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
  "com_com_pod"    : [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
  "dragon_friends" : [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
  "jl_show"        : [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
  "night_vale"     : [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  "radiolab"       : [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
  "serial"         : [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
  "tofop"          : [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
}

"""
Given the name of a podcast, returns the vector encoding of that podcast
"""
def pod_to_vec(pod):
  return encodings[pod]

"""
Given a file name of a podcast spectrogram, 
returns the vector encoding of the podcast in 
"""
def fname_to_vec(fname):
  match = re.search("/([a-zA-Z_]+)_[0-9]+_snip[0-9]+\.png$", fname)
  return encodings[match.group(1)]

"""
Given a one-hot vector, returns the name of the associated podcast
"""
def vec_to_pod(vec):
  for (key, val) in encodings.items():
    if val == vec:
      return key


"""
For testing purposes
"""
def main(argv):
  
  vec = pod_to_vec("behind_lines")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  vec = pod_to_vec("bugle")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  vec = pod_to_vec("campaign")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  vec = pod_to_vec("com_com_pod")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  vec = pod_to_vec("dragon_friends")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  vec = pod_to_vec("tofop")
  print(vec)
  pod = vec_to_pod(vec)
  print(pod)
  
  print( fname_to_vec("../spects_1/behind_lines_1_snip8.png") )
  

if __name__ == "__main__":
  main(sys.argv)






