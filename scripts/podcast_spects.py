""" 
Contains helper functions to load and label podcast spectrograms
"""
import sys, glob
import tensorflow as tf
import resize_image_patch
from podcast_encodings import *


def get_label_file_list(file_pattern, test_on_unseen_eps=False):
  """
  Returns a list of labels and filenames of relevant images matching
  the given file pattern (glob).
  Also, splits data into train/test sets 
  (not sure where else to do this...)
  """
  files = glob.glob(file_pattern)
  if files == []:
    raise FileNotFoundError("Could not find files matching "+file_pattern)
  
  if test_on_unseen_eps:
    print( "WARNING: Testing only on unseen podcast episodes" )
  
  train_labels = []
  train_files  = []
  test_labels  = []
  test_files   = []
  for f in files:    
    if test_on_unseen_eps:
      # Podcast episodes ending are added to the test set entirely
      if re.search("9_snip[0-9]+\.png$", f):
        test_labels.append(fname_to_vec(f))
        test_files.append(f)
      else:
        train_labels.append(fname_to_vec(f))
        train_files.append(f)    
          
    else:
      # Snippets from each podcast episode are put into the test set
      if re.search("snip1?0\.png$", f):
        test_labels.append(fname_to_vec(f))
        test_files.append(f)
      else:
        train_labels.append(fname_to_vec(f))
        train_files.append(f)
  
  #print( test_files )
  print( "Images loaded from:", file_pattern )
  print( len(train_files), "training,", len(test_files), "test items")
  return train_labels, train_files, test_labels, test_files


def read_and_decode_image(input_queue, flatten=False, num_channels=1,
                          img_height=257, img_width=500, top_crop=20):
  """
  Load images from a queue and do simple preprocessing
  """
  label = input_queue[1]
  # Read the image
  file_data = tf.read_file(input_queue[0])
  image = tf.image.decode_png(file_data, channels=num_channels)
  # Pad the image to the correct size
  image = tf.image.resize_image_with_crop_or_pad(image, 
                 img_height, img_width, dynamic_shape=True)
  image.set_shape([img_height, img_width, num_channels])
  
  if top_crop > 0:
    # Crop some less useful pixels from the top of the image,
    # Since these are mostly black
    image = tf.image.crop_to_bounding_box(image, 
                                  top_crop, 0, 
                                  img_height-top_crop, img_width)
  if flatten:
    # image is flattened into an array, most structural info is lost
    image = tf.reshape(image, [-1])
  
  #Scale image values to between 0 and 1
  image = tf.to_float(image)/255
  
  return label, image



"""
For testing purposes
"""
def main(argv):
  
  train_labels, train_files, test_labels, test_files = \
    get_label_file_list("../spects/medium/dr*.png")
  #print("train labels:", train_labels)
  #print("train files:", train_files)
  #print("test labels:", test_labels)
  #print("test files:", test_files)
  
  train_imgs_t = tf.convert_to_tensor(train_files, dtype=tf.string)
  train_labs_t = tf.convert_to_tensor(train_labels, dtype=tf.float32)
  test_imgs_t  = tf.convert_to_tensor(test_files, dtype=tf.string)
  test_labs_t  = tf.convert_to_tensor(test_labels, dtype=tf.float32)
  
  # create input queues
  train_input_queue = tf.train.slice_input_producer(
    [train_imgs_t, train_labs_t], shuffle=False)
  test_input_queue = tf.train.slice_input_producer(
    [test_imgs_t, test_labs_t], shuffle=False)
  
  # get single examples
  _, train_image = read_and_decode_image(train_input_queue)
  _, test_image = read_and_decode_image(test_input_queue)
  
  print(train_image)
  print(test_image)
  
  _, train_image_flat = read_and_decode_image(train_input_queue, 
                                              flatten=True, top_crop=0)
  
  print(train_image_flat)
  
if __name__ == "__main__":
  main(sys.argv)






