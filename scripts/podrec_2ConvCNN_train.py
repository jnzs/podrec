"""
A podcast spectrogram classifier.

Trains a smal convolutional neural network to predict which podcast 
series a 15-second sound snippet comes from (presented as a spectrogram)
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import resize_image_patch
import re, glob, argparse

import podcast_spects as spect

#IMG_DIR = "../spects/medium"
NUM_OUTPUTS = 10
#IMG_HEIGHT = 257
#IMG_WIDTH  = 500
NUM_CHANNELS = 1 # Use greyscale images

#BATCH_SIZE = 200

# Some useful functions from the Tensorflow tutorials
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)


# Create CNN model
class CNN_model:
  """
  A class that defines the convolutional neural network structure
  """
  def __init__(self, enable_dropout=False, 
               conv1_feat_maps=16,
               conv2_feat_maps=32,
               fc1_units=1024):
    # initialise weight variables
    
    # First convolutional layer weights and biases
    # NOTE: Shape is of the form: 
    #   [filter_size_x, filter_size_y, input channels, output_channels]
    self.W_conv1 = weight_variable([7, 7, 1, conv1_feat_maps])
    self.b_conv1 = bias_variable([conv1_feat_maps])
    print("conv1 feature maps:", conv1_feat_maps)
    # Second convolutional layer weights and biases
    self.W_conv2 = weight_variable([5, 5, conv1_feat_maps, conv2_feat_maps])
    self.b_conv2 = bias_variable([conv2_feat_maps])
    print("conv2 feature maps:", conv2_feat_maps)
    # First fully connected layer weights and biases
    self.W_fc1 = weight_variable([7 * 14 * conv2_feat_maps, fc1_units])
    self.b_fc1 = bias_variable([fc1_units])
    # Second fully connected layer weights and biases
    self.W_fc2 = weight_variable([fc1_units, 512])
    self.b_fc2 = bias_variable([512])
    # Output layer weights and biases
    self.W_out = weight_variable([512, 10])
    self.b_out = bias_variable([10])
    # Placeholder for dropout rate
    self.keep_prob = tf.placeholder(tf.float32)
    self.enable_dropout = enable_dropout
    self.conv2_feat_maps = conv2_feat_maps
      
  def inference(self, images):
    # build model
    
    #print("images:", images)
    #print("W_Conv1", self.W_conv1.get_shape())
    
    # First convolutional layer
    conv1 = tf.nn.conv2d(images, self.W_conv1, 
                         strides=[1, 3, 3, 1], padding='SAME')
    rect_conv1 = tf.nn.relu(conv1 + self.b_conv1)
    #print("Conv1:", rect_conv1)
    # First max pool layer
    pool1 = tf.nn.max_pool(rect_conv1, ksize=[1, 3, 3, 1], 
                           strides=[1, 3, 3, 1], padding='SAME')
    #print("Pool1:", pool1)
    
    # Second convolutional layer
    conv2 = tf.nn.conv2d(pool1, self.W_conv2, 
                         strides=[1, 2, 2, 1], padding='SAME')
    rect_conv2 = tf.nn.relu(conv2 + self.b_conv2)
    #print("Conv2:", rect_conv2)
    # Second max pool layer
    pool2 = tf.nn.max_pool(rect_conv2, ksize=[1, 2, 2, 1], 
                           strides=[1, 2, 2, 1], padding='SAME')
    #print("Pool2:", pool2)
    
    # Flatten output for use in fully connected layers
    flat_pool2 = tf.reshape(pool2, [-1, 7 * 14 * self.conv2_feat_maps])
    #print("flat_pool2:", flat_pool2)
    
    # First fully connected layer
    fc1 = tf.nn.relu(tf.matmul(flat_pool2, self.W_fc1) + self.b_fc1)
    if self.enable_dropout:
      fc1 = tf.nn.dropout(fc1, self.keep_prob)
      
    print("fc1:", fc1)
    # Second fully connected layer
    fc2 = tf.nn.relu(tf.matmul(fc1, self.W_fc2) + self.b_fc2)
    if self.enable_dropout:
      fc2 = tf.nn.dropout(fc2, self.keep_prob)
    print("fc2:", fc2)
    # Output layer
    output = tf.nn.softmax(tf.matmul(fc2, self.W_out) + self.b_out)
    #print("out:", output)
    return output  


# Create CNN with less subsampling and an additional convolutional layer
class Deeper_CNN_model:
  """
  A class that defines a deeper CNN structure with 3 convolutional layers
  """
  def __init__(self, enable_dropout=False, 
               conv1_feat_maps=32,
               fc1_units=1024):
    # initialise weight variables
    print("Using deeper CNN architechture")
    # First convolutional layer weights and biases
    # NOTE: Shape is of the form: 
    #   [filter_size_x, filter_size_y, input channels, output_channels]
    self.W_conv1 = weight_variable([7, 7, 1, conv1_feat_maps])
    self.b_conv1 = bias_variable([conv1_feat_maps])
    #print("conv1 feature maps:", conv1_feat_maps)
    # Second convolutional layer weights and biases
    self.W_conv2 = weight_variable([5, 5, conv1_feat_maps, 32])
    self.b_conv2 = bias_variable([32])
    # Third convolutional layer weights and biases
    self.W_conv3 = weight_variable([3, 3, 32, 32])
    self.b_conv3 = bias_variable([32])
    # First fully connected layer weights and biases
    self.W_fc1 = weight_variable([8 * 16 * 32, fc1_units])
    self.b_fc1 = bias_variable([fc1_units])
    # Second fully connected layer weights and biases
    self.W_fc2 = weight_variable([fc1_units, 512])
    self.b_fc2 = bias_variable([512])
    # Output layer weights and biases
    self.W_out = weight_variable([512, 10])
    self.b_out = bias_variable([10])
    # Placeholder for dropout rate
    self.keep_prob = tf.placeholder(tf.float32)
    self.enable_dropout = enable_dropout
      
  def inference(self, images):
    # build model
    
    #print("images:", images)
    #print("W_Conv1", self.W_conv1.get_shape())
    
    # First convolutional layer
    conv1 = tf.nn.conv2d(images, self.W_conv1, 
                         strides=[1, 2, 2, 1], padding='SAME')
    rect_conv1 = tf.nn.relu(conv1 + self.b_conv1)
    print("Conv1:", rect_conv1)
    # First max pool layer
    pool1 = tf.nn.max_pool(rect_conv1, ksize=[1, 2, 2, 1], 
                           strides=[1, 2, 2, 1], padding='SAME')
    print("Pool1:", pool1)
    
    # Second convolutional layer
    conv2 = tf.nn.conv2d(pool1, self.W_conv2, 
                         strides=[1, 2, 2, 1], padding='SAME')
    rect_conv2 = tf.nn.relu(conv2 + self.b_conv2)
    print("Conv2:", rect_conv2)
    # Second max pool layer
    pool2 = tf.nn.max_pool(rect_conv2, ksize=[1, 2, 2, 1], 
                           strides=[1, 2, 2, 1], padding='SAME')
    print("Pool2:", pool2)
    
    # Third convolutional layer
    conv3 = tf.nn.conv2d(pool2, self.W_conv3, 
                         strides=[1, 1, 1, 1], padding='SAME')
    rect_conv3 = tf.nn.relu(conv3 + self.b_conv3)
    print("Conv3:", rect_conv3)
    # Second max pool layer
    pool3 = tf.nn.max_pool(rect_conv3, ksize=[1, 2, 2, 1], 
                           strides=[1, 2, 2, 1], padding='SAME')
    print("Pool3:", pool3)
    
    # Flatten output for use in fully connected layers
    flat_pool3 = tf.reshape(pool3, [-1, 8 * 16 * 32])
    print("flat_pool3:", flat_pool3)
    
    # First fully connected layer
    fc1 = tf.nn.relu(tf.matmul(flat_pool3, self.W_fc1) + self.b_fc1)
    if self.enable_dropout:
      fc1 = tf.nn.dropout(fc1, self.keep_prob)
      
    print("fc1:", fc1)
    # Second fully connected layer
    fc2 = tf.nn.relu(tf.matmul(fc1, self.W_fc2) + self.b_fc2)
    if self.enable_dropout:
      fc2 = tf.nn.dropout(fc2, self.keep_prob)
    print("fc2:", fc2)
    # Output layer
    output = tf.nn.softmax(tf.matmul(fc2, self.W_out) + self.b_out)
    print("out:", output)
    return output 


def main():
  
  parser = argparse.ArgumentParser()
  parser.add_argument("-n", "--num_iterations", type=int, default=1000)
  parser.add_argument("-b", "--batch_size", type=int, default=200)
  parser.add_argument("-i", "--img_dir", default='../spects/medium', 
                      help="directory of training images")
  parser.add_argument("-x", "--img_width", default=500, 
                      type=int)
  parser.add_argument("-y", "--img_height", default=257, 
                      type=int)
  parser.add_argument("-o", "--outfile", 
                      help="file of saved model weights")

  args = parser.parse_args()
  
  # get data
  train_labels, train_files, test_labels, test_files = \
    spect.get_label_file_list(args.img_dir+"/*.png",      
                              test_on_unseen_eps=True)
  # convert to tensors
  train_imgs_t = tf.convert_to_tensor(train_files, dtype=tf.string)
  train_labs_t = tf.convert_to_tensor(train_labels, dtype=tf.float32)
  test_imgs_t  = tf.convert_to_tensor(test_files, dtype=tf.string)
  test_labs_t  = tf.convert_to_tensor(test_labels, dtype=tf.float32)
  
  # create input queue
  train_input_queue = tf.train.slice_input_producer(
    [train_imgs_t, train_labs_t], shuffle=True)
  # don't bother to shuffle the test set - we use all of it every time
  test_input_queue = tf.train.slice_input_producer(
    [test_imgs_t, test_labs_t], shuffle=False)
  
  # get single examples (preprocessed images)
  train_label, train_image = spect.read_and_decode_image( 
                             train_input_queue, flatten=False,
                             img_height=args.img_height, img_width=args.img_width)
  test_label, test_image = spect.read_and_decode_image(
                           test_input_queue, flatten=False,
                           img_height=args.img_height, img_width=args.img_width)
  
  # group training examples into batches randomly
  print("Batch size:", args.batch_size)
  train_images_batch, train_labels_batch = tf.train.batch(
      [train_image, train_label], batch_size=args.batch_size,
      capacity=600)
  # The test batch contains the whole test set
  test_images_batch, test_labels_batch = tf.train.batch(
      [test_image, test_label], batch_size=len(test_files),
      capacity=2*len(test_files))
  
  # TODO IMPORTANT Construct model
  cnn = CNN_model(enable_dropout=True, conv1_feat_maps=32, conv2_feat_maps=32)
  # cnn = Deeper_CNN_model(enable_dropout=True, conv1_feat_maps=32)
  
  train_output = cnn.inference(train_images_batch)
  test_output = cnn.inference(test_images_batch)
  
  # Cross entropy (loss function)
  train_x_entropy = tf.reduce_mean(
    -tf.reduce_mean(train_labels_batch * tf.log(train_output+1e-8), 1) )
  train_op = tf.train.AdamOptimizer(0.001).minimize(
    train_x_entropy)
  
  test_x_entropy = tf.reduce_mean(
    -tf.reduce_mean(test_labels_batch * tf.log(test_output+1e-8), 1) )
  
  # Define some stuff for validation
  train_accuracy = tf.reduce_mean(tf.cast(
    tf.equal(tf.argmax(train_output,1), tf.argmax(train_labels_batch,1)), 
    tf.float32))
  test_accuracy = tf.reduce_mean(tf.cast(
    tf.equal(tf.argmax(test_output,1), tf.argmax(test_labels_batch,1)), 
    tf.float32))

  # Initializer op
  init = tf.initialize_all_variables()
  
  # Saver object keeps track of weight variables to save them for later
  if args.outfile != None:
    saver = tf.train.Saver()
  
  with tf.Session() as sess:
    # First initialise tf variables
    sess.run(init)
    
    # initialise queue runners and coordinator
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)
    
    # Iterate TODO make more efficient?
    for i in range(args.num_iterations):
      sess.run(train_op, feed_dict={cnn.keep_prob: 0.5})
      if i%20 == 0: 
        cost, acc = sess.run([train_x_entropy, train_accuracy], 
                              feed_dict={cnn.keep_prob:1.0})
        print("Iteration", i, ":", cost)
      if i%100 == 0:
        test_cost, test_acc = sess.run([test_x_entropy, test_accuracy],
                                        feed_dict={cnn.keep_prob: 1.0})
        print("Training accuracy (estimate):", str(acc*100)+'%')
        print("Test accuracy:", str(test_acc*100)+'%')
        if args.outfile != None:
          save_path = saver.save(sess, args.outfile)
          print("Model saved in file: %s" % save_path)
    print("Finished after", i+1, "iterations")
    
    # test/validate model on test data
    cost, acc, _, _ = sess.run([train_x_entropy, train_accuracy, train_labels_batch, train_output], feed_dict={cnn.keep_prob: 1.0})
    test_cost, test_acc = sess.run([test_x_entropy, test_accuracy], feed_dict={cnn.keep_prob: 1.0})
    print("Final training cost (estimate):", cost)
    print("Final test cost:", test_cost)
    print("Final training accuracy (estimate):", str(acc*100)+'%')
    print("Final test accuracy:", str(test_acc*100)+'%')
    if args.outfile != None:
      save_path = saver.save(sess, args.outfile)
      print("Final model saved in file: %s" % save_path)
    
    # Sanity check
    #print("----- Random Target Outputs: -----")
    #print(tar[0])
    #print(tar[1])
    #print(tar[2])
    #print(tar[3])
    #print(tar[4])
    #print("----- Random Actual Outputs: -----")
    #print(out[0])
    #print(out[1])
    #print(out[2])
    #print(out[3])
    #print(out[4])
    
    # unitialise queue runners and coordinator
    coord.request_stop()
    coord.join(threads)
    

if __name__ == "__main__":
  main()
