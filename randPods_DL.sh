#!/bin/sh

# Given a podcast rss feed, downloads random episodes from that podcast
feed=$1
num_eps=$2
out_prefix=$3
start_no=$4

if [ -z "$feed" -o -z "$num_eps" -o -z "$out_prefix" ]; then
  echo "usage: randPods_DL.sh <rss/atom_feed> <num_episodes> <file_prefix> (<start_no>)"
  exit 1;
fi

# Generate list of URLS of random episodes
eps=$(python3 ./randPods.py $feed $num_eps)

if [ -z $start_no ]; then
  count=0
else
  count=$start_no
fi
# Download eps, naming them
for ep in $eps; do
  echo $ep
  echo "saved as ${out_prefix}_$count.mp3"
  wget -O "${out_prefix}_$count.mp3" $ep
  count=$(($count+1))
done
